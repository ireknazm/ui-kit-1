import { create } from '@storybook/theming';

export default create({
  base: 'light',

  brandTitle: 'MediaPult UI Components',
  brandUrl: '/',
  brandImage: '/logo-light.png',
});
